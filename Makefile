TARGET_EXEC ?= i3dropdown

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src

PREFIX = /usr

SRCS := $(shell find $(SRC_DIRS) -name *.cc)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= -std=c++17 -fno-rtti -fno-exceptions  -fno-threadsafe-statics \
        -pipe -Wconversion-null -msse3 -O2 $(INC_FLAGS) -MMD -MP
CC = gcc

all : $(BUILD_DIR)/$(TARGET_EXEC)

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(CPPFLAGS) $(OBJS) -o $@ $(LDFLAGS)

# c++ source
$(BUILD_DIR)/%.cc.o: %.cc
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: install
install: $(BUILD_DIR)/$(TARGET_EXEC)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $(BUILD_DIR)/$(TARGET_EXEC) $(DESTDIR)$(PREFIX)/bin/$(TARGET_EXEC)

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(TARGET_EXEC)

.PHONY: musl-static

musl-static: CC = musl-gcc -static
musl-static: all

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p

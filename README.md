## I3Dropdown

I3Dropdown is a fast and highly configurable window toggler with animations.
Given a window name and a launch command, I3Dropdown will toggle the visiblity
of the window with the given name by animating it. If the no window with the 
name exists the launch command is used to start it.

## Installation 

Building:
```
$ git clone https://gitlab.com/exrok/i3dropdown.git
$ cd i3dropdown
$ make
$ sudo make install
```

## Usage 

Add the following to the i3 config to insure the socket-path is quickly
accessible. 
```
exec --no-startup-id i3 --get-socketpath > /tmp/i3wm-socket-path
```

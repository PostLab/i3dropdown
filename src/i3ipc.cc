#include <stdio.h>
#include <string.h>

#include <inttypes.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "i3ipc.h"

unsigned char *get_i3tree(int sockfd){
    if (ipc_send_message(sockfd, 0, I3_IPC_MESSAGE_TYPE_GET_TREE, NULL) == -1){
            fprintf(stderr, "IPC: read()");
            exit(1);
    }
    uint32_t reply_length;
    uint32_t reply_type;
    uint8_t *reply;

    if (ipc_recv_message(sockfd, &reply_type, &reply_length, &reply) != 0) {
            fprintf(stderr, "IPC: read()");
            exit(1);
    }
    return reply;

}

size_t writeall(int fd, const unsigned char *buf, size_t count) {
    size_t written = 0;
    ssize_t n = 0;

    while (written < count) {
        n = write(fd, buf + written, count - written);
        if (n == -1) {
            if (errno == EINTR || errno == EAGAIN)
                continue;
            return n;
        }
        written += (size_t)n;
    }

    return written;
}

char *sstrdup(const char *str) {
    char *result = strdup(str);
    if (result == NULL)
        abort();
    return result;
}
/*
 * Formats a message (payload) of the given size and type and sends it to i3 via
 * the given socket file descriptor.
 *
 * Returns -1 when write() fails, errno will remain.
 * Returns 0 on success.
 *
 */
int ipc_send_message(int sockfd, const uint32_t message_size,
                     const uint32_t message_type, const uint8_t *payload) {
    const i3_ipc_header_t header = {
        /* We don’t use I3_IPC_MAGIC because it’s a 0-terminated C string. */
        .magic = {'i', '3', '-', 'i', 'p', 'c'},
        .size = message_size,
        .type = message_type};
    if (writeall(sockfd, (unsigned char*)((void *)&header), sizeof(i3_ipc_header_t)) == -1)
        return -1;

    if (writeall(sockfd, payload, message_size) == -1)
        return -1;

    return 0;
}

int i3ipc_xrun(int sockfd, const uint32_t message_size, const char *payload) {
    if (writeall(sockfd, (uint8_t *) payload, message_size) == -1)
        return -1;

    return 0;
}
int i3ipc_run(int sockfd, const uint32_t message_size, const unsigned char *payload){
   return ipc_send_message(sockfd, message_size,I3_IPC_MESSAGE_TYPE_RUN_COMMAND, (uint8_t *) payload); 
}
/*
 * Connects to the i3 IPC socket and returns the file descriptor for the
 * socket. die()s if anything goes wrong.
 *
 */
/*
 * Reads a message from the given socket file descriptor and stores its length
 * (reply_length) as well as a pointer to its contents (reply).
 *
 * Returns -1 when read() fails, errno will remain.
 * Returns -2 on EOF.
 * Returns -3 when the IPC protocol is violated (invalid magic, unexpected
 * message type, EOF instead of a message). Additionally, the error will be
 * printed to stderr.
 * Returns 0 on success.
 *
 */
int ipc_recv_message(int sockfd, uint32_t *message_type,
                     uint32_t *reply_length, uint8_t **reply) {
    /* Read the message header first */

    const uint32_t to_read = strlen(I3_IPC_MAGIC) + sizeof(uint32_t) + sizeof(uint32_t);
    char msg[to_read];
    char *walk = msg;

    uint32_t read_bytes = 0;
    while (read_bytes < to_read) {
        int n = read(sockfd, msg + read_bytes, to_read - read_bytes);
        if (n == -1)
            return -1;
        if (n == 0) {
            return -2;
        }

        read_bytes += n;
    }

    if (memcmp(walk, I3_IPC_MAGIC, strlen(I3_IPC_MAGIC)) != 0) {
        return -3;
    }

    walk += strlen(I3_IPC_MAGIC);
    memcpy(reply_length, walk, sizeof(uint32_t));
    walk += sizeof(uint32_t);
    if (message_type != NULL)
        memcpy(message_type, walk, sizeof(uint32_t));

    *reply = (unsigned char *)malloc(*reply_length);

    read_bytes = 0;
    int n;
    while (read_bytes < *reply_length) {
        if ((n = read(sockfd, *reply + read_bytes, *reply_length - read_bytes)) == -1) {
            if (errno == EINTR || errno == EAGAIN)
                continue;
            return -1;
        }

        read_bytes += n;
    }
    (*reply)[*reply_length - 1] = '\0';

    return 0;
}

int ipc_connect(const char *socket_path) {
    int sockfd = socket(AF_LOCAL, SOCK_STREAM, 0);
    if (sockfd == -1)
        return -1;

    (void)fcntl(sockfd, F_SETFD, FD_CLOEXEC);

    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_LOCAL;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
    if (connect(sockfd, (const struct sockaddr *)&addr, sizeof(struct sockaddr_un)) < 0)
        return -1;

    return sockfd;
}

int ipc_connect() {
    char path[128];
    int fpd = open("/tmp/i3wm-socket-path", O_RDONLY, 0);
    int nx = read(fpd, path,100);

    if (fpd == -1 || nx < 0){
       fprintf(stderr,
"ERROR: `/tmp/i3wm-socket-path`, could not be read. Please add\n \
\n\
exec --no-startup-id i3 --get-socketpath > /tmp/i3wm-socket-path\n\
\n\
  too the i3 config and either run the command or restart i3.\n\n\
  This is recommended since `i3 --get-socketpath` is relatively slow and the\n\
  cache must be updated if i3 restarts."); 
       exit(1);
    }


    path[nx-1] = '\0';
    int socketfd = ipc_connect(path);

    if (socketfd == -1){
        fprintf(stderr,"ERROR: trying to connect to i3 ipc with socket path: \"%s\"\n", path);
        fprintf(stderr,"Please ensure `/tmp/i3wm-socket-path` is upto date. and\n");
        fprintf(stderr,"   exec --no-startup-id i3 --get-socketpath > /tmp/i3wm-socket-path\n");
        fprintf(stderr,"is the i3 config.");
        abort();
    }
    return socketfd;
}

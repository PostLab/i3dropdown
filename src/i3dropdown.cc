#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#include "i3ipc.h"
#include "i3con.h"
#include "prediquery.h"

#define VERSION "0.9.0"

struct rect_box {
   long int width, height, x, y;
};

rect_box rect_parse(char * json){
    rect_box box;
    using namespace prediquery;
    QueryExpr query[] = {
        /*0*/invoice("x", &box.x),
        /*1*/invoice("y", &box.y), 
        /*2*/invoice("width", &box.width), 
        /*3*/invoice("height", &box.height)
    };
    uint64_t res = tquery(json, query, 4, bitset(0, 1, 2, 3));
    if (res != bitset(0,1,2,3)){
        printf("rect parse error");
    }
    return box;
}
typedef int (*ease_t)(int, int, int);

int linear_ease(int current, int added, int max){
    float td = ((float)current / (float)max);
    return (float)added*(td);
}
int quint_ease(int current, int added, int max){
    float td = ((float)current / (float)max) - 1.0;
    return (float)added*(td*td*td*td*td + 1.0);
}

int cubic_ease(int current, int added, int max){
    float td = ((float)current / (float)max) - 1.0;
    return (float)added*(td*td*td + 1.0);
}

int quad_ease(int current, int added, int max){
    float td = ((float)current / (float)max);
    return (float)-added*(td*(td - 2)) ;
}


ease_t parse_ease(const char *str){
    switch (atoi(str)){
        case 0: return linear_ease;
        case 1: return quad_ease;
        case 2: return cubic_ease;
        case 3: return quint_ease;
        default: 
            fprintf(stderr,"Invalid Ease Function\n");
            abort();
    }
}

class EaseFunc {
    int total_frames, offset, value, sign;
    ease_t ease;
    bool forward_ease; //ease_in vs ease_out
    public:
    EaseFunc(int total_frames, int from, int to, int (*ease)(int,int,int),
             bool forward_ease = false):
        total_frames{total_frames},
        offset{from},
        value{abs(to - from)},
        sign{(to - from) < 0 ? -1 : 1},
        ease{ease},
        forward_ease{forward_ease}{}

    int operator()(int current) const {
        if (forward_ease){
            return offset + sign * ease(current, value, total_frames);
        }else{
            int reversed_value = ease(total_frames - current, value, total_frames);
            return offset + sign * (value - reversed_value);
        }
    }
};

void shell_exec(char *com_str){
    char * const shell_argv[4] = {(char *)"/bin/sh", (char *)"-c", com_str, NULL }; 
    if (fork() == 0 ) execv("/bin/sh", shell_argv);
}

struct i3Query{
    int64_t instance_workspace;
    int64_t current_workspace;
    bool instance_found;
    uint64_t instance_id;
    rect_box output;
};

i3Query queryi3(const char * inst, const char *reply){
    char *output_rect = nullptr;
    int64_t instance_workspace = 0;
    int64_t current_workspace = 0;
    uint64_t instance_id = 0;

    using namespace prediquery;
    const QueryExpr query[] = {
       /*00*/recurse("nodes", bitset(1, 2, 3)), //Output Nodes
       /*01*/invoice("rect", &output_rect, bitset(2)),  
       /*02*/dummy(bitset(1, 6), raise_flag(2)), //FOUND Focus Dummy Flag

       /*03*/recurse("nodes", bitset(2, 4, 11)), //{topdock, *content*, bottomdock} Nodes

       /*04*/recurse("nodes", bitset(2, 5, 6, 7, 8, 9, 11)), //Workspaces Nodes
       /*05*/invoice("num", &instance_workspace, bitset(7)), 
       /*06*/invoice("num", &current_workspace, bitset(2, 11), EXPR_FLAG::OR),
       /*07*/dummy(bitset(5), raise_flag(7)), //FOUND Instance Dummy Flag

       /*08*/recurse("floating_nodes", bitset(2, 7, 8, 9, 10, 11, 12)), //Container Nodes
       /*09*/recurse("nodes",          bitset(2, 7, 8, 9, 10, 11, 12)), //Container Nodes
       /*10*/invoice("id", &instance_id, bitset(13)), 
       /*11*/predicate("focused", true, bitset(6), EXPR_FLAG::BOOL | raise_flag(2)),

       /*12*/recurse("window_properties", bitset(13)), //Container Nodes window properties
       /*13*/predicate("instance", inst, bitset(10, 7), EXPR_FLAG::STRING_PREFIX | EXPR_FLAG::RAISE_RELATED)
    };

    uint64_t stub = tquery(reply, query, 14 /*length of query[]*/, bitset(0));

    if ((stub & bitset(1, 6)) != bitset(1, 6)){
        fprintf(stderr,"Could not find active output/workspace\n");
        exit(1);
    }

    return i3Query{
        .instance_workspace = instance_workspace,
        .current_workspace = current_workspace,
        .instance_found = (stub & bitset(10, 5)) == bitset(10, 5),
        .instance_id = instance_id,
        .output = rect_parse(output_rect)
    };
}

struct SizeDef {
    int percent, min, max;

    int calculate(int base){
        int res = (base * percent)/100;
        if (max > 0 && max <= res) return max;
        if (min > 0 && min >= res) return min;
        return res;
    }
};

SizeDef parse_bounds (const char *str){
   SizeDef bound;
   bound.percent = atoi(optarg);

   if (!(optarg = strchr(optarg, ','))) return bound;
   ++optarg;
   bound.min = atoi(optarg);

   if (!(optarg = strchr(optarg, ','))) return bound;
   ++optarg;
   bound.max = atoi(optarg);
   return bound;
}

enum Position {
    TOP = 0,
    LEFT = 0,
    CENTER = 1,
    BOTTOM = 2,
    RIGHT = 2
};

int calc_offset(Position pos, int base_offset, int base_size, int size){
    switch (pos){
       case 0: //Top or Left 
           return base_offset;
       case 1: //Center
           return base_offset + (base_size / 2) - size/2;
       case 2: //Bottom or right
           return base_offset + base_size - size;
       default: //Should never happen;
           return 0;
    }
}


void print_usage(){
    static const char * DOCS_STRING =
        "i3dropdown " VERSION
R"XXX(

Usage:
   i3dropdown [options] <instance_name> <command>

<instance_name>: Is the first entry of WM_CLASS on the x11 window that is going
  to be toggled. The first matching instancing found is used.

<command>: Command to start the application with instance_name if the
  application is not found to be running.

Options:
   -e <hide-ease>[,show-ease]   Specifies easing functions for animating the
   (default: 2,3)               window, using a numerical scheme as follows,
                                {0:linear, 1:quadratic, 2: cubic, 3: quintic}.

   -f <fps>                     Animate at <fps> frames per second 
   (default: 60)

   -h, -?                       Print this help message

   -H <percent>[,min][,max]     Defines the Height/Width of the window:
   (default: 65,0,0)            <percent>: represented by a number from 1 to 100
                                defines percentage height/width of the window
   -W <percent>[,min][,max]     relative to the current output.
   (default: 100,0,0)           <min>/<max>: are the minimum and maximum 
                                height/width of the window in pixels, values of 
                                zero are ignored.

   -l <N>                       Defines the length of the animation in the number
   (default: 10)                frames. 

   -p <X><Y>                    Defines the relative position of the shown window 
   (default: CT)                with respect to displaying output, input as two 
                                uppercase letters in the scheme as follows:
                                X:{L:left, C:center, R:right},
                                Y:{T:top, C:center, B:bottom} 

   -u                           Makes windows drop up instead of down.

Example:
   i3dropdown ddterminal 'urxvtc -name ddterminal' 

   Provides a dropdown terminal using urxvtc. Note: the instance name must be 
   tagged as floating, in this case, that can be accomplished by the add the 
   following to your i3 config. 

   for_window [instance="^ddterminal$"] floating enable;
)XXX";
    puts(DOCS_STRING);
}

struct Config {
    bool hidden_below;
    int fps, frames;
    Position y_pos, x_pos;
    SizeDef width, height;
    ease_t show_ease, hide_ease;
};

Config parse_arguments(Config &&config, int argc, char **argv){
    opterr = 0;
    optind = 0;
    int c;
    while ((c = getopt (argc, argv, "ul:f:H:W:p:e:")) != -1)
        switch(c){
            case 'l':
                config.frames = atoi(optarg);
                continue;
            case 'u':
                config.hidden_below = true;
                continue;
            case '?'://fallthrough
            case 'h':
                print_usage();
                exit(0);
            case 'W':
                config.width = parse_bounds(optarg);
                continue;
            case 'H':
                config.height = parse_bounds(optarg);
                continue;
            case 'f':
                config.fps = atoi(optarg);
                continue;
            case 'e':
                config.show_ease = parse_ease(optarg);
                optarg  = strchr(optarg,',');

                if (!optarg) continue;
                c = atoi(optarg + 1);

                config.hide_ease = parse_ease(optarg + 1);
                continue;
            case 'p':
                switch (optarg[0]){
                    case 'L': config.x_pos = LEFT; break;
                    case 'R': config.x_pos = RIGHT; break;
                    case 'C': config.x_pos = CENTER; break;
                    default:
                        fprintf(stderr,"Unknown Position\n");
                        abort();
                }
                switch (optarg[1]){
                    case 'T': config.y_pos = TOP; break;
                    case 'C': config.y_pos = CENTER; break;
                    case 'B': config.y_pos = BOTTOM; break;
                    case '\0': break;
                    default:
                        fprintf(stderr,"Unknown Position\n");
                        abort();
                }
                continue;
        }
    return config;
}

int main( int argc, char *argv[] ){
    Config config = parse_arguments({
        .hidden_below = false,
        .fps = 60,
        .frames = 10,
        .y_pos = TOP,
        .x_pos = CENTER,
        .width = SizeDef{.percent = 100, .min = 0, .max = 0},
        .height = SizeDef{.percent = 65, .min = 0, .max = 0},
        .show_ease = cubic_ease,
        .hide_ease = quint_ease,
    }, argc, argv);

    if (argc - optind != 2){
        fprintf(stderr,"ERROR:Missing required arguments, use -h flag for usage/help.\n");
        exit(1);
    }

    char *instance_name = argv[0 + optind]; 
    char *shell_command = argv[1 + optind]; 

    int sockfd = ipc_connect();

    char *i3tree =(char *)get_i3tree(sockfd);
    i3Query i3 = queryi3(instance_name, i3tree);
    free(i3tree);
    
    I3Window dropdown{sockfd};
    bool hiding;
    if (i3.instance_found){
        hiding = (i3.instance_workspace != i3.current_workspace);
        dropdown.target_id(i3.instance_id);
    } else {
        hiding = true;
        dropdown.target_instance(instance_name);

        shell_exec(shell_command);
    }
    
    int window_width = config.width.calculate(i3.output.width),
        window_height = config.height.calculate(i3.output.height),
        window_x = calc_offset(config.x_pos, i3.output.x, i3.output.width, window_width),
        window_y = calc_offset(config.y_pos, i3.output.y, i3.output.height, window_height);

    int FUZZ = 18; //I3wm will not render a window if it is off the output
    int hidden_y = config.hidden_below ? i3.output.y - FUZZ + i3.output.height
                                       : i3.output.y + FUZZ - window_height;
    const EaseFunc ease{
            config.frames,                                
            hiding ? hidden_y         : window_y,         
            hiding ? window_y         : hidden_y,         
            hiding ? config.hide_ease : config.show_ease, 
            hiding                                        
    };

    dropdown.show();
    dropdown.resize(window_width, window_height);

    timespec timer{.tv_nsec = 1000000000l / config.fps};

    for (int frame = 1; frame <= config.frames - 1; ++frame) {
        dropdown.position(window_x, ease(frame));
        if (frame < config.frames) nanosleep(&timer, NULL);
    }

    if (!hiding) dropdown.hide(); //Move Window to Scratchpad

    close(sockfd);
}


